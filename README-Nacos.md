# Sentinel 1.7.1

## 1. 改造sentinel控制台（对nacos的支持）
生产环境中我们将sentinel控制台的规则管理数据保存到nacos中，就需要对sentinel控制台进行源码修改，下面我们来讨论修改的具体方案。

## 2. 规则数据保存规范
根据nacos和sentinel控制台的特性我们将主要对流控规则、降级规则、热点规则、系统规则和授权规则5种不同后缀的Data ID。采用“服务名称.规则后缀”+namespace的方式将数据保存到nacos。
规则后缀如下：

规则名称 |  文件后缀  | namespace | 服务名称 | Data ID 
--------|------------|-----------|---------|-----------
流控规则 |.flow      | 86d97c2f-c250-4ddc-983f-e211d1e41b75 | resource2 | resource2.flow
降级规则 |.degrade   | 86d97c2f-c250-4ddc-983f-e211d1e41b75 | resource2 | resource2.degrade
热点规则 |.paramflow | 86d97c2f-c250-4ddc-983f-e211d1e41b75 | resource2 | resource2.paramflow
系统规则 |.system    | 86d97c2f-c250-4ddc-983f-e211d1e41b75 | resource2 | resource2.system
授权规则 |.authority | 86d97c2f-c250-4ddc-983f-e211d1e41b75 | resource2 | resource2.authority

![](./images/rule-exraple.png)


## 3. sentinel控制台操作原理
sentinel控制台设置和查看规则原理。例如服务resource2（ip=127.0.0.1，prot=8080，name=resource2）的规则操作。

### 3.1。 查看规则
**默认逻辑：**
- 1.根据resource2的ip、port、name去查询客户端配置的规则数据（规则数据是保存在客户端）listRules
- 2.将listRules数据保存在sentinel控制台内存
- 3.将listRules数据返回给查询页面

**改造nacos支持逻辑：**

- 1.根据resource2的ip、port、name和注册nacos的namespace和group封装nacos的规则结合当前具体操作的规则类型封装nacos的Data ID，ruleDataId
- 2.将ruleDataId去查询nacos的规则数据（规则数据是保存在nacos）listRules
- 3.将listRules数据保存在sentinel控制台内存
- 4.将listRules数据返回给查询页面


### 3.2. 新增、修改和删除规则
**默认逻辑：**
- 1.将当前持久化（新增、修改和删除）的数据保存到sentinel控制台内存
- 2.从内存中查询当前规则的所有数据listRules
- 3.将上一步的规则数据listRules推送到远程客户端resource2来修改resource2的规则

**改造nacos支持逻辑：**
- 1.将当前持久化（新增、修改和删除）的数据保存到sentinel控制台内存
- 2.从内存中查询当前规则的所有数据listRules
- 3.根据resource2的ip、port、name和注册nacos的namespace和group封装nacos的规则结合当前具体操作的规则类型封装nacos的Data ID，ruleDataId
- 4.将第2步的规则数据listRules推送到第3步nacos的ruleDataId
- 5.nacos配置中心将ruleDataId数据推送到订阅ruleDataId的客户端实现更新客户端的规则


## 4. sentinel控制台代码修改实战
根据官方给出获取信息接口DynamicRuleProvider<T>和发布信息接口DynamicRulePublisher<T>来改造

```java
public interface DynamicRuleProvider<T> {
    /**
     * 通过当前资源的服务名称appName获取规则数据
     * @param appName 当前资源的服务名称
     */    
    T getRules(String appName) throws Exception;
}
```

```java
public interface DynamicRulePublisher<T> {

    /**
     * 将当前资源的当前规则推送给客户端
     * @param app   当前资源的服务名称
     * @param rules 当前资源内存中的所有规则数据
     * @throws Exception if some error occurs
     */
    void publish(String app, T rules) throws Exception;
}
```

### 4.1 定义nacos的配置常量
```java
public class NacosConfigConstant {
    /**
     * 流控规则文件后缀
     */
    public static final String FLOW_POSTFIX = ".flow";

    /**
     * 降流规则文件后缀
     */
    public static final String DEGRADE_POSTFIX = ".degrade";

    /**
     * 授权规则文件后缀
     */
    public static final String AUTHORITY_POSTFIX = ".authority";

    /**
     * 系统规则文件后缀
     */
    public static final String SYSTEM_POSTFIX = ".system";

    /**
     * 热点规则文件后缀
     */
    public static final String PARAM_FLOW_POSTFIX = ".paramflow";


    /**
     * 默认GROUP等于DEFAULT_GROUP
     */
    public static final String GROUP_ID = "DEFAULT_GROUP";


    /**
     * 默认IP地址
     */
    public static final String IP = "localhost";

    /**
     * 默认端口号
     */
    public static final String PORT = "8848";

    /**
     * 默认namespace等于public
     */
    public static final String NAMESPACE = "";
}
```

### 4.2 定义nacos的配置文件
```java
@Component("nacosConfigProperties")
@ConfigurationProperties(prefix = "nacos.server")
public class NacosConfigProperties {

    private String ip = NacosConfigConstant.IP;

    private String port = NacosConfigConstant.PORT;

    private String namespace=NacosConfigConstant.NAMESPACE;

    private String groupId = NacosConfigConstant.GROUP_ID;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getServerAddr() {
        return this.getIp() + ":" + this.getPort();
    }

    @Override
    public String toString() {
        return "NacosConfigProperties [ip=" + ip + ", port=" + port + ", namespace="
                + namespace + ", groupId=" + groupId + "]";
    }

}
```

### 4.3. nacos服务相关Bean配置

```java
@Configuration
public class NacosConfig {

    @Autowired
    private NacosConfigProperties nacosConfigProperties;

    /**
     * 配置规则的编码器和解码器 Bean
     */
    @Bean
    public Converter<List<FlowRuleEntity>, String> flowRuleEntityEncoder() {
        return rules -> JSON.toJSONString(rules, true);
    }

    @Bean
    public Converter<String, List<FlowRuleEntity>> flowRuleEntityDecoder() {
        return s -> JSON.parseArray(s, FlowRuleEntity.class);
    }


    @Bean
    public Converter<List<ParamFlowRuleEntity>, String> paramFlowRuleEntityEncoder() {
        return rules -> JSON.toJSONString(rules, true);
    }

    @Bean
    public Converter<String, List<ParamFlowRuleEntity>> paramFlowRuleEntityDecoder() {
        return s -> JSON.parseArray(s, ParamFlowRuleEntity.class);
    }


    @Bean
    public Converter<List<DegradeRuleEntity>, String> degradeRuleEntityEncoder() {
        return rules -> JSON.toJSONString(rules, true);
    }

    @Bean
    public Converter<String, List<DegradeRuleEntity>> degradeRuleEntityDecoder() {
        return s -> JSON.parseArray(s, DegradeRuleEntity.class);
    }


    @Bean
    public Converter<List<AuthorityRuleEntity>, String> authorityRuleEntityEncoder() {
        return rules -> JSON.toJSONString(rules, true);
    }

    @Bean
    public Converter<String, List<AuthorityRuleEntity>> authorityRuleEntityDecoder() {
        return s -> JSON.parseArray(s, AuthorityRuleEntity.class);
    }


    @Bean
    public Converter<List<SystemRuleEntity>, String> systemRuleEntityEncoder() {
        return rules -> JSON.toJSONString(rules, true);
    }

    @Bean
    public Converter<String, List<SystemRuleEntity>> systemRuleEntityDecoder() {
        return s -> JSON.parseArray(s, SystemRuleEntity.class);
    }


    /**
     * 配置 Nacos服务ConfigService
     *
     * @return
     * @throws Exception
     */
    @Bean
    public ConfigService nacosConfigService() throws Exception {
        Properties properties = new Properties();
        properties.put(PropertyKeyConst.SERVER_ADDR, nacosConfigProperties.getServerAddr());
        properties.put(PropertyKeyConst.NAMESPACE, nacosConfigProperties.getNamespace());
        return ConfigFactory.createConfigService(properties);
    }

}
```
### 4.4. DynamicRuleProvider接口具体实现
定义AbstractRuleNacosProvider统一实现的基础类,每一个具体规则继承该类即可
```java
/**
 * @author luodea
 * @date 2020/4/6 11:44
 * @description 编写动态推拉模式的扩展代码
 */

public abstract class AbstractRuleNacosProvider<T> implements DynamicRuleProvider<List<T>> {


    private static Logger logger = LoggerFactory.getLogger(AbstractRuleNacosProvider.class);

    public static final long DEFAULT_TIMEOUT = 3000;

    private long timeoutMs = DEFAULT_TIMEOUT;

    @Autowired
    private NacosConfigProperties nacosConfigProperties;

    @Autowired
    private ConfigService nacosConfigService;


    private Converter<String, List<T>> ruleEntityDecoder;


    @Override
    public List<T> getRules(String app) throws Exception {
        String dataId = getDataId(app);
        String rules = nacosConfigService.getConfig(dataId, nacosConfigProperties.getGroupId(), timeoutMs);
        logger.info("从Nacos,Ip={},port={},Namespace={},Group={},DataId={}中拉取规则信息:{}",
                nacosConfigProperties.getIp(), nacosConfigProperties.getPort(), nacosConfigProperties.getNamespace(), nacosConfigProperties.getGroupId(), dataId, rules);
        if (StringUtil.isEmpty(rules)) {
            return new ArrayList<>();
        }
        return getRuleEntityDecoder().convert(rules);
    }


    /**
     * 获取Nacos的Data ID
     *
     * @param app
     * @return
     */
    protected abstract String getDataId(String app);


    public Converter<String, List<T>> getRuleEntityDecoder() {
        return ruleEntityDecoder;
    }

    public void setRuleEntityDecoder(Converter<String, List<T>> ruleEntityDecoder) {
        this.ruleEntityDecoder = ruleEntityDecoder;
    }

    public long getTimeoutMs() {
        return timeoutMs;
    }

    public void setTimeoutMs(long timeoutMs) {
        this.timeoutMs = timeoutMs;
    }
}
```
#### 4.4.1. 如流控规则(FlowRuleNacosProvider)的具体实现，其他规则同理
```java
/**
 * @author luodea
 * @date 2020/4/9 10:04
 * @description 流控规则对DynamicRuleProvider的具体实现
 */
@Component
public class FlowRuleNacosProvider extends AbstractRuleNacosProvider<FlowRuleEntity> implements InitializingBean {

    /**
     * 注入流控规则解码器
     */
    @Autowired
    Converter<String, List<FlowRuleEntity>> flowRuleEntityDecoder;

    @Override
    public void afterPropertiesSet() throws Exception {
        setRuleEntityDecoder(flowRuleEntityDecoder);
    }

    /**
     * 生成流控规则在Nacos中的Data ID
     * @param app
     * @return
     */
    @Override
    protected String getDataId(String app) {
        return app + NacosConfigConstant.FLOW_POSTFIX;
    }

}
```

### 4.5 DynamicRulePublisher接口具体实现
定义AbstractRuleNacosPublisher统一实现的基础类,每一个具体规则继承该类即可
```java
/**
 * @author luodea
 * @date 2020/4/6 11:46
 * @description 编写动态推送模式的扩展代码
 */

@Component
public abstract class AbstractRuleNacosPublisher<T> implements DynamicRulePublisher<List<T>> {

    private static Logger logger = LoggerFactory.getLogger(AbstractRuleNacosPublisher.class);

    @Autowired
    private NacosConfigProperties nacosConfigProperties;

    @Autowired
    private ConfigService nacosConfigService;

    private Converter<List<T>, String> ruleEntityEncoder;


    @Override
    public void publish(String app, List<T> rules) throws Exception {
        String dataId = getDataId(app);
        AssertUtil.notEmpty(dataId, "app name cannot be empty");
        String convert = getRuleEntityEncoder().convert(rules);
        logger.info("给Nacos,Ip={},port={},Namespace={},Group={},DataId={}更新规则信息:{}",
                nacosConfigProperties.getIp(), nacosConfigProperties.getPort(), nacosConfigProperties.getNamespace(), nacosConfigProperties.getGroupId(), dataId, convert);
        if (rules == null) {
            return;
        }
        nacosConfigService.publishConfig(dataId, nacosConfigProperties.getGroupId(), convert);
        Thread.sleep(200);
    }


    /**
     * 获取Nacos的Data ID
     *
     * @param app
     * @return
     */
    protected abstract String getDataId(String app);

    public Converter<List<T>, String> getRuleEntityEncoder() {
        return ruleEntityEncoder;
    }

    public void setRuleEntityEncoder(Converter<List<T>, String> ruleEntityEncoder) {
        this.ruleEntityEncoder = ruleEntityEncoder;
    }
}
```

#### 4.5.1. 如流控规则(FlowRuleNacosPublisher)的具体实现，其他规则同理
```java
/**
 * @author luodea
 * @date 2020/4/9 10:59
 * @description 流控规则数据更新发布DynamicRulePublisher的具体实现
 */
@Component
public class FlowRuleNacosPublisher extends AbstractRuleNacosPublisher<FlowRuleEntity> implements InitializingBean {


    /**
     * 注入流控规则数据的编码器
     */
    @Autowired
    private Converter<List<FlowRuleEntity>, String> flowRuleEntityEncoder;


    @Override
    public void afterPropertiesSet() throws Exception {
        setRuleEntityEncoder(flowRuleEntityEncoder);
    }

    /**
     * 生成流控规则在Nacos中的Data ID
     *
     * @param app
     * @return
     */
    @Override
    protected String getDataId(String app) {
        return app + NacosConfigConstant.FLOW_POSTFIX;
    }
}
```

## 5. 修改sentinel控制器
将DynamicRuleProvider和DynamicRulePublisher的具体实现注入到sentinel控制器中。sentinel控制器有哪些看下图
![](./images/resource.png)



### 5.1. 修改sentinel降流规则控制器（DegradeController）

#### 5.1.1. 注入降流的DegradeRuleNacosProvider和DegradeRuleNacosPublisher

```
    /*****************************  Nacos支持配置  *************************************/
    @Autowired
    private DegradeRuleNacosProvider degradeRuleNacosProvider;
    @Autowired
    private DegradeRuleNacosPublisher degradeRuleNacosPublisher;
```

#### 5.1.2. 修改查询方法
```
    @ResponseBody
    @RequestMapping("/rules.json")
    @AuthAction(PrivilegeType.READ_RULE)
    public Result<List<DegradeRuleEntity>> queryMachineRules(String app, String ip, Integer port) {

        if (StringUtil.isEmpty(app)) {
            return Result.ofFail(-1, "app can't be null or empty");
        }
        if (StringUtil.isEmpty(ip)) {
            return Result.ofFail(-1, "ip can't be null or empty");
        }
        if (port == null) {
            return Result.ofFail(-1, "port can't be null");
        }
        try {

            //  List<DegradeRuleEntity> rules =  sentinelApiClient.fetchDegradeRuleOfMachine(app, ip, port); 默认
            /**
             * 修改对nacos的支持
             */
            List<DegradeRuleEntity> rules = degradeRuleNacosProvider.getRules(app);
            rules = repository.saveAll(rules);
            return Result.ofSuccess(rules);
        } catch (Throwable throwable) {
            logger.error("queryApps error:", throwable);
            return Result.ofThrowable(-1, throwable);
        }
    }
```

#### 5.1.3. 修改发布方法
```
    private boolean publishRules(String app, String ip, Integer port) {
        List<DegradeRuleEntity> rules = repository.findAllByMachine(MachineInfo.of(app, ip, port)); //默认
        // return sentinelApiClient.setDegradeRuleOfMachine(app, ip, port, rules);  //默认
        try {
            // 将规则推送到Nacos中
            degradeRuleNacosPublisher.publish(app, rules);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
```

**到处控制器修改完成。其他规则修改也一样。**
