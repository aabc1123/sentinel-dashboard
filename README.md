# Sentinel 1.7.1

## 0. 概述

**sentinel**是阿里巴巴推出一款针对微服务流量控制、降级熔断、系统资源保护的开源产品。我相信很多人对它并不陌生，我也相信有人会有疑问？为什么要选择使用sentinel？
答案很简单：
- sentinel本来是阿里巴巴一个内部项目，它诞生于2012年主要负责阿里巴巴其他项目的入口流量控制，特别是这几年在淘宝和天猫上的使用证明了它的性能和可行性。
- 和当前主流的其他产品（netflix的hystrix）相比，它功能更加强大、还提供控制台管理系统，能实时监控和动态修改规则。
- 对java主流框架进行了适配如web servlet、webflux、gateway、zuul、feign、dubbo等。让使用变得更加简单。
- [官方文档](https://github.com/alibaba/Sentinel/wiki/%E4%BB%8B%E7%BB%8D)很完整。

## 1. 目的
既然sentinel的官方文档很完善了，那本文的目的又有是什么呢？

- sentinel控制台功能强大，但是所有数据只保存在内存中，一旦程序重启我们所有的规则设置将全部丢失。官方对于[生产环境的使用](https://github.com/alibaba/Sentinel/wiki/%E5%9C%A8%E7%94%9F%E4%BA%A7%E7%8E%AF%E5%A2%83%E4%B8%AD%E4%BD%BF%E7%94%A8-Sentinel)也给出了明确的解决方案,但是需要我们手动修改源码来完成
- 讨论sentinel在spring cloud中的使用实践
- sentinel控制台的push 模式的数据源进行了对**nacos**的支持的扩展，对nacos不了解建议先去看[官方文档](https://nacos.io/zh-cn/docs/what-is-nacos.html)。
- 本文不讨论sentinel是什么？干什么？怎么样？，如果还有这些疑问建议先去看[官方文档](https://github.com/alibaba/Sentinel/wiki/%E4%BB%8B%E7%BB%8D)。

## 2. 相关文档

### [Sentinel 控制台启动](./README-START.md)

### [Sentinel控制器改造Nacos的支持](./README-Nacos.md)

### [Sentinel在spring cloud中的使用实践](./README-SPRING-CLOUD.md)