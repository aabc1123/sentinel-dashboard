# Sentinel 1.7.1 在spring cloud中的使用

## 1. 导入依赖jar

```
<!-- sentinel 依赖 -->
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
</dependency>

<!-- sentinel 动态数据源对Nacos的支持  -->
<dependency>
    <groupId>com.alibaba.csp</groupId>
    <artifactId>sentinel-datasource-nacos</artifactId>
</dependency>
```

## 2. 配置文件配置数据源

```
# nacos 服务相关配置
spring.application.name=@artifactId@
spring.cloud.nacos.server-addr=nacos:8848
spring.cloud.nacos.config.file-extension=yaml
spring.cloud.nacos.config.shared-configs= ${spring.application.name}.${spring.cloud.nacos.config.file-extension}


# sentinel的namespace
sentinel.nacos.namespace=86d97c2f-c250-4ddc-983f-e211d1e41b75
# 流控规则数据源配置
spring.cloud.sentinel.datasource.flow.nacos.server-addr=${spring.cloud.nacos.server-addr}
spring.cloud.sentinel.datasource.flow.nacos.namespace=${sentinel.nacos.namespace}
spring.cloud.sentinel.datasource.flow.nacos.data-id=${spring.application.name}.flow
spring.cloud.sentinel.datasource.flow.nacos.rule-type=flow
# 热点规则数据源配置
spring.cloud.sentinel.datasource.paramflow.nacos.server-addr=${spring.cloud.nacos.server-addr}
spring.cloud.sentinel.datasource.paramflow.nacos.namespace=${sentinel.nacos.namespace}
spring.cloud.sentinel.datasource.paramflow.nacos.data-id=${spring.application.name}.paramflow
spring.cloud.sentinel.datasource.paramflow.nacos.rule-type=param_flow
# 降级规则数据源配置
spring.cloud.sentinel.datasource.degrade.nacos.server-addr=${spring.cloud.nacos.server-addr}
spring.cloud.sentinel.datasource.degrade.nacos.namespace=${sentinel.nacos.namespace}
spring.cloud.sentinel.datasource.degrade.nacos.data-id=${spring.application.name}.degrade
spring.cloud.sentinel.datasource.degrade.nacos.rule-type=degrade
# 系统规则数据源配置
spring.cloud.sentinel.datasource.system.nacos.server-addr=${spring.cloud.nacos.server-addr}
spring.cloud.sentinel.datasource.system.nacos.namespace=${sentinel.nacos.namespace}
spring.cloud.sentinel.datasource.system.nacos.data-id=${spring.application.name}.system
spring.cloud.sentinel.datasource.system.nacos.rule-type=system
# 授权规则数据源配置
spring.cloud.sentinel.datasource.authority.nacos.server-addr=${spring.cloud.nacos.server-addr}
spring.cloud.sentinel.datasource.authority.nacos.namespace=${sentinel.nacos.namespace}
spring.cloud.sentinel.datasource.authority.nacos.data-id=${spring.application.name}.authority
spring.cloud.sentinel.datasource.authority.nacos.rule-type=authority
```


## 3. 全局异常处理
一般情况当我们需要对请求资源被限流统一处理时，该功能就能完美的解决，实际开发中也经常这样使用。
sentinel是在进行流控制时，如果请求资源的次数超出预定规则则会发生BlockException异常，具体规则异常如下表：

规则名称 | 异常对象
--------|--------
流控规则 | FlowException
热点规则 | ParamFlowException
降流规则 | DegradeException
系统规则 | SystemBlockException
授权规则 | AuthorityException

### 3.1. web servlet 全局异常处理
sentinel提供一个BlockExceptionHandler接口，只要在spring boot中定义BlockExceptionHandler的Bean即可完成sentinel全局异常处理的实现。

```java
/**
 * @author luodea
 * @date 2020/4/5 17:10
 * @description sentinel异常统一处理抽象类，对返回的结果进行了统一的封装
 */
public abstract class AbstractBlockExceptionHandler implements BlockExceptionHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, BlockException e) throws Exception {
        StringBuffer url = request.getRequestURL();
        if (StringUtil.isNotBlank(request.getQueryString())) {
            url.append("?").append(request.getQueryString());
        }
        //将异常信息返回到前端
        HttpUtils.FlowRequest(response, handleBlockException(request, e, url.toString()));
    }

    /**
     * 处理限流异常信息
     *
     * @param request 当前请求
     * @param e       限流具体异常
     * @param url     当前请求URL
     * @return 返回给前端的信息
     */
    protected abstract String handleBlockException(HttpServletRequest request, BlockException e, String url);

}


/**
 * @author luodea
 * @date 2020/4/5 17:02
 * @description sentinel 全局异常处理
 */
@Slf4j
public class GlobalBlockExceptionHandler extends AbstractBlockExceptionHandler {

    @Override
    protected String handleBlockException(HttpServletRequest request, BlockException e, String url) {
        String msg = SentinelUtils.HandlerException(e);
        log.info(url + "  ---->  " + msg);
        return msg;
    }
}

/**
 * @author luodea
 * @date 2020/4/10 13:18
 * @description Sentinel工具类
 */
public class SentinelUtils {

    public static String HandlerException(BlockException e) {
        String msg = null;
        if (e instanceof DegradeException) {
            msg = "因被降级处理，已被限流";
        }

        if (e instanceof FlowException) {
            msg = "因超过最大流量阈值，已被限流";
        }

        if (e instanceof ParamFlowException) {
            msg = "因超热点流量超过阈值，已被限流";
        }

        if (e instanceof SystemBlockException) {
            msg = "因系统负载过高，已被限流";
        }

        if (e instanceof AuthorityException) {
            msg = "因当前访问来源origin，已在黑白名单或不在白名单，已被限流";
        }
        return msg;
    }
    
}

```
只要在spring boot中添加GlobalBlockExceptionHandler的Bean定义，即可完成




## 3.2. gateway全局异常处理
gateway是基于webflux实现的，通过webflux的全局异常接口WebExceptionHandler实现全局异常处理，实践代码如下：

```java

@Configuration
public class GatewayConfiguration {
    @Bean
    public WebExceptionHandler sentinelGatewayBlockExceptionHandler() {
        return new GlobalWebExceptionHandler();
    }
}

public class GlobalWebExceptionHandler implements WebExceptionHandler {
    @Override
    public Mono<Void> handle(ServerWebExchange exchange, Throwable ex) {
        if (exchange.getResponse().isCommitted()) {
            return Mono.error(ex);
        }
        if (!BlockException.isBlockException(ex)) {
            return HttpUtils.Write(exchange, RestResult.RESULT(ResultStatus.SYSTEM_EXCEPTION));
        }
        return writeAndFlushWith(exchange, (BlockException) ex);
    }


    /**
     * 封装返回数据发布器
     *
     * @param exchange
     * @param ex
     * @return
     */
    private Mono<Void> writeAndFlushWith(ServerWebExchange exchange, BlockException ex) {
        return HttpUtils.Write(exchange, RestResult.RESULT(ResultStatus.TOO_MANY_REQUESTS, SentinelUtils.HandlerException(ex)));
    }

}

public class HttpUtils {

    /**
     * HttpServletResponse 返回数据
     *
     * @param response
     * @param status
     * @param body
     */
    public static void Write(HttpServletResponse response, int status, String body) {
        response.setStatus(status);
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        try {
            PrintWriter printWriter = response.getWriter();
            printWriter.write(body == null ? "" : body);
            printWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 未登录时返回JSON数据
     *
     * @param response
     */
    public static void UnLoginWrite(HttpServletResponse response) {
        Result restResult = RestResult.RESULT(ResultStatus.UN_LOGIN);
        Write(response, restResult.getCode(), JSON.toJSONString(restResult));
    }

    /**
     * 限流异常时返回的JSON数据
     *
     * @param response
     * @param message  返回信息
     */
    public static void FlowRequest(HttpServletResponse response, String message) {
        Result restResult = message == null ? RestResult.RESULT(ResultStatus.TOO_MANY_REQUESTS) : RestResult.RESULT(ResultStatus.TOO_MANY_REQUESTS, message);
        Write(response, restResult.getCode(), JSON.toJSONString(restResult));
    }

    /**
     * 包装Mono<DataBuffer>对象
     *
     * @param result
     * @return
     */
    public static Mono<DataBuffer> Wrap(Result result) {
        DefaultDataBufferFactory defaultDataBufferFactory = new DefaultDataBufferFactory();
        String jsonString = JSON.toJSONString(result);
        return Mono.just(defaultDataBufferFactory.wrap(jsonString.getBytes()));
    }

    /**
     * 返回数据
     *
     * @param exchange
     * @param result
     * @return
     */
    public static Mono<Void> Write(ServerWebExchange exchange, Result result) {
        ServerHttpResponse response = exchange.getResponse();
        /**
         * 设置状态码为429
         */
        response.setRawStatusCode(result.getCode());
        /**
         * 设置返回信息为JSON类型
         */
        response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
        return response.writeAndFlushWith(Mono.just(Wrap(result)));
    }
}

```







## 3. sentinel集成openfeign使用
配置文件打开 Sentinel 对 Feign 的支持
```
feign.sentinel.enabled=true
```
加入 spring-cloud-starter-openfeign 依赖使 Sentinel starter 中的自动化配置类生效：
```
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>
```

实践案例
```java
/**
 * @author luodea
 * @date 2020/3/22 13:18
 * @description
 */
@FeignClient(name = "resource1",contextId = "serverService",fallback = ServerServiceFallback.class,configuration = FeignConfiguration.class)
public interface ServerService {


    String RESOURCE_URI = "server";

    /**
     * 获取resource1服务的地址和端口
     * @return
     */
    @GetMapping(RESOURCE_URI)
    RestResult serverInfo();

}

public class ServerServiceFallback implements ServerService {
    @Override
    public RestResult serverInfo() {
        return (RestResult)RestResult.FAIL("您已被限流");
    }
}

@Configuration
public class FeignConfiguration {

    @Bean
    public ServerServiceFallback serverServiceFallback() { return new ServerServiceFallback();
    }

}
```

注意：当配置了**全局异常处理**和**openfeign支持**时，会被openfeign处理给覆盖全局异常失效。



