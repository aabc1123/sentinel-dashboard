package com.alibaba.csp.sentinel.dashboard.nacos.publisher;

import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.FlowRuleEntity;
import com.alibaba.csp.sentinel.dashboard.nacos.NacosConfigConstant;
import com.alibaba.csp.sentinel.datasource.Converter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author luodea
 * @date 2020/4/9 10:59
 * @description 流控规则数据更新发布DynamicRulePublisher的具体实现
 */
@Component
public class FlowRuleNacosPublisher extends AbstractRuleNacosPublisher<FlowRuleEntity> implements InitializingBean {


    /**
     * 注入流控规则数据的编码器
     */
    @Autowired
    private Converter<List<FlowRuleEntity>, String> flowRuleEntityEncoder;


    @Override
    public void afterPropertiesSet() throws Exception {
        setRuleEntityEncoder(flowRuleEntityEncoder);
    }

    /**
     * 生成流控规则在Nacos中的Data ID
     *
     * @param app
     * @return
     */
    @Override
    protected String getDataId(String app) {
        return app + NacosConfigConstant.FLOW_POSTFIX;
    }
}
