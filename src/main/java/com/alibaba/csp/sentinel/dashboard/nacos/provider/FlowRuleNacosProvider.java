package com.alibaba.csp.sentinel.dashboard.nacos.provider;

import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.FlowRuleEntity;
import com.alibaba.csp.sentinel.dashboard.nacos.NacosConfigConstant;
import com.alibaba.csp.sentinel.datasource.Converter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author luodea
 * @date 2020/4/9 10:04
 * @description 流控规则对DynamicRuleProvider的具体实现
 */
@Component
public class FlowRuleNacosProvider extends AbstractRuleNacosProvider<FlowRuleEntity> implements InitializingBean {

    /**
     * 注入流控规则解码器
     */
    @Autowired
    Converter<String, List<FlowRuleEntity>> flowRuleEntityDecoder;

    @Override
    public void afterPropertiesSet() throws Exception {
        setRuleEntityDecoder(flowRuleEntityDecoder);
    }

    /**
     * 生成流控规则在Nacos中的Data ID
     * @param app
     * @return
     */
    @Override
    protected String getDataId(String app) {
        return app + NacosConfigConstant.FLOW_POSTFIX;
    }

}
