package com.alibaba.csp.sentinel.dashboard.nacos.publisher;

import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.AuthorityRuleEntity;
import com.alibaba.csp.sentinel.dashboard.nacos.NacosConfigConstant;
import com.alibaba.csp.sentinel.datasource.Converter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author luodea
 * @date 2020/4/9 10:59
 * @description
 */
@Component
public class AuthorityRuleNacosPublisher extends AbstractRuleNacosPublisher<AuthorityRuleEntity> implements InitializingBean {


    @Autowired
    private Converter<List<AuthorityRuleEntity>, String> authorityRuleEntityEncoder;


    @Override
    public void afterPropertiesSet() throws Exception {
        setRuleEntityEncoder(authorityRuleEntityEncoder);
    }

    @Override
    protected String getDataId(String app) {
        return app + NacosConfigConstant.AUTHORITY_POSTFIX;
    }
}
