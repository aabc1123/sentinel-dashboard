package com.alibaba.csp.sentinel.dashboard.nacos.provider;

import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.ParamFlowRuleEntity;
import com.alibaba.csp.sentinel.dashboard.nacos.NacosConfigConstant;
import com.alibaba.csp.sentinel.datasource.Converter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author luodea
 * @date 2020/4/9 10:04
 * @description
 */
@Component
public class ParamFlowRuleNacosProvider extends AbstractRuleNacosProvider<ParamFlowRuleEntity> implements InitializingBean {

    @Autowired
    Converter<String, List<ParamFlowRuleEntity>> paramFlowRuleEntityEncoder;

    @Override
    public void afterPropertiesSet() throws Exception {
        setRuleEntityDecoder(paramFlowRuleEntityEncoder);
    }

    @Override
    protected String getDataId(String app) {
        return app + NacosConfigConstant.PARAM_FLOW_POSTFIX;
    }
}
