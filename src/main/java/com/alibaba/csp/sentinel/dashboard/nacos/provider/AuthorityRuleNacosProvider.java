package com.alibaba.csp.sentinel.dashboard.nacos.provider;

import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.AuthorityRuleEntity;
import com.alibaba.csp.sentinel.dashboard.nacos.NacosConfigConstant;
import com.alibaba.csp.sentinel.datasource.Converter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author luodea
 * @date 2020/4/9 10:04
 * @description
 */
@Component
public class AuthorityRuleNacosProvider extends AbstractRuleNacosProvider<AuthorityRuleEntity> implements InitializingBean {

    @Autowired
    Converter<String, List<AuthorityRuleEntity>> authorityRuleEntityDecoder;


    @Override
    public void afterPropertiesSet() throws Exception {
        setRuleEntityDecoder(authorityRuleEntityDecoder);
    }

    @Override
    protected String getDataId(String app) {
        return app + NacosConfigConstant.AUTHORITY_POSTFIX;
    }

}
