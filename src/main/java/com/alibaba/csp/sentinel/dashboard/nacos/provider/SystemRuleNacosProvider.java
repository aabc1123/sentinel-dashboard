package com.alibaba.csp.sentinel.dashboard.nacos.provider;

import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.SystemRuleEntity;
import com.alibaba.csp.sentinel.dashboard.nacos.NacosConfigConstant;
import com.alibaba.csp.sentinel.datasource.Converter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author luodea
 * @date 2020/4/9 10:04
 * @description
 */
@Component
public class SystemRuleNacosProvider extends AbstractRuleNacosProvider<SystemRuleEntity> implements InitializingBean {

    @Autowired
    Converter<String, List<SystemRuleEntity>> systemRuleEntityDecoder;

    @Override
    public void afterPropertiesSet() throws Exception {
        setRuleEntityDecoder(systemRuleEntityDecoder);
    }

    @Override
    protected String getDataId(String app) {
        return app + NacosConfigConstant.SYSTEM_POSTFIX;
    }

}
