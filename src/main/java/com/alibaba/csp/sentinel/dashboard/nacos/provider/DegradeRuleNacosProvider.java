package com.alibaba.csp.sentinel.dashboard.nacos.provider;

import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.AuthorityRuleEntity;
import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.DegradeRuleEntity;
import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.RuleEntity;
import com.alibaba.csp.sentinel.dashboard.nacos.NacosConfigConstant;
import com.alibaba.csp.sentinel.datasource.Converter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author luodea
 * @date 2020/4/9 10:04
 * @description
 */
@Component
public class DegradeRuleNacosProvider extends AbstractRuleNacosProvider<DegradeRuleEntity> implements InitializingBean {

    @Autowired
    Converter<String, List<DegradeRuleEntity>> degradeRuleEntityDecoder;


    @Override
    public void afterPropertiesSet() throws Exception {
        setRuleEntityDecoder(degradeRuleEntityDecoder);
    }


    @Override
    protected String getDataId(String app) {
        return app + NacosConfigConstant.DEGRADE_POSTFIX;
    }

}
