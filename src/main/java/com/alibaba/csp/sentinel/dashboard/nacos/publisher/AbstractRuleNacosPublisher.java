package com.alibaba.csp.sentinel.dashboard.nacos.publisher;

import com.alibaba.csp.sentinel.dashboard.nacos.NacosConfigProperties;
import com.alibaba.csp.sentinel.dashboard.rule.DynamicRulePublisher;
import com.alibaba.csp.sentinel.datasource.Converter;
import com.alibaba.csp.sentinel.util.AssertUtil;
import com.alibaba.nacos.api.config.ConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author luodea
 * @date 2020/4/6 11:46
 * @description 编写动态推送模式的扩展代码
 */

@Component
public abstract class AbstractRuleNacosPublisher<T> implements DynamicRulePublisher<List<T>> {

    private static Logger logger = LoggerFactory.getLogger(AbstractRuleNacosPublisher.class);

    @Autowired
    private NacosConfigProperties nacosConfigProperties;

    @Autowired
    private ConfigService nacosConfigService;

    private Converter<List<T>, String> ruleEntityEncoder;


    @Override
    public void publish(String app, List<T> rules) throws Exception {
        String dataId = getDataId(app);
        AssertUtil.notEmpty(dataId, "app name cannot be empty");
        String convert = getRuleEntityEncoder().convert(rules);
        logger.info("给Nacos,Ip={},port={},Namespace={},Group={},DataId={}更新规则信息:{}",
                nacosConfigProperties.getIp(), nacosConfigProperties.getPort(), nacosConfigProperties.getNamespace(), nacosConfigProperties.getGroupId(), dataId, convert);
        if (rules == null) {
            return;
        }
        nacosConfigService.publishConfig(dataId, nacosConfigProperties.getGroupId(), convert);
        Thread.sleep(200);
    }


    /**
     * 获取Nacos的Data ID
     *
     * @param app
     * @return
     */
    protected abstract String getDataId(String app);

    public Converter<List<T>, String> getRuleEntityEncoder() {
        return ruleEntityEncoder;
    }

    public void setRuleEntityEncoder(Converter<List<T>, String> ruleEntityEncoder) {
        this.ruleEntityEncoder = ruleEntityEncoder;
    }
}
