package com.alibaba.csp.sentinel.dashboard.nacos;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author luodea
 * @date 2020/4/6 11:40
 * @description nacos 服务参数配置
 */
@Component("nacosConfigProperties")
@ConfigurationProperties(prefix = "nacos.server")
public class NacosConfigProperties {


    private String ip = NacosConfigConstant.IP;

    private String port = NacosConfigConstant.PORT;

    private String namespace=NacosConfigConstant.NAMESPACE;

    private String groupId = NacosConfigConstant.GROUP_ID;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getServerAddr() {
        return this.getIp() + ":" + this.getPort();
    }

    @Override
    public String toString() {
        return "NacosConfigProperties [ip=" + ip + ", port=" + port + ", namespace="
                + namespace + ", groupId=" + groupId + "]";
    }


}
