package com.alibaba.csp.sentinel.dashboard.nacos.publisher;

import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.ParamFlowRuleEntity;
import com.alibaba.csp.sentinel.dashboard.nacos.NacosConfigConstant;
import com.alibaba.csp.sentinel.datasource.Converter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author luodea
 * @date 2020/4/9 10:59
 * @description
 */
@Component
public class ParamFlowRuleNacosPublisher extends AbstractRuleNacosPublisher<ParamFlowRuleEntity> implements InitializingBean {


    @Autowired
    private Converter<List<ParamFlowRuleEntity>, String> paramFlowRuleEntityEncoder;


    @Override
    public void afterPropertiesSet() throws Exception {
        setRuleEntityEncoder(paramFlowRuleEntityEncoder);
    }


    @Override
    protected String getDataId(String app) {
        return app + NacosConfigConstant.PARAM_FLOW_POSTFIX;
    }
}
