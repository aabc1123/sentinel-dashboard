package com.alibaba.csp.sentinel.dashboard.nacos.provider;

import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.AuthorityRuleEntity;
import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.RuleEntity;
import com.alibaba.csp.sentinel.dashboard.nacos.NacosConfigProperties;
import com.alibaba.csp.sentinel.dashboard.rule.DynamicRuleProvider;
import com.alibaba.csp.sentinel.datasource.Converter;
import com.alibaba.csp.sentinel.util.StringUtil;
import com.alibaba.nacos.api.config.ConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * @author luodea
 * @date 2020/4/6 11:44
 * @description 编写动态推拉模式的扩展代码
 */

public abstract class AbstractRuleNacosProvider<T> implements DynamicRuleProvider<List<T>> {


    private static Logger logger = LoggerFactory.getLogger(AbstractRuleNacosProvider.class);

    public static final long DEFAULT_TIMEOUT = 3000;

    private long timeoutMs = DEFAULT_TIMEOUT;

    @Autowired
    private NacosConfigProperties nacosConfigProperties;

    @Autowired
    private ConfigService nacosConfigService;


    private Converter<String, List<T>> ruleEntityDecoder;


    @Override
    public List<T> getRules(String app) throws Exception {
        String dataId = getDataId(app);
        String rules = nacosConfigService.getConfig(dataId, nacosConfigProperties.getGroupId(), timeoutMs);
        logger.info("从Nacos,Ip={},port={},Namespace={},Group={},DataId={}中拉取规则信息:{}",
                nacosConfigProperties.getIp(), nacosConfigProperties.getPort(), nacosConfigProperties.getNamespace(), nacosConfigProperties.getGroupId(), dataId, rules);
        if (StringUtil.isEmpty(rules)) {
            return new ArrayList<>();
        }
        return getRuleEntityDecoder().convert(rules);
    }


    /**
     * 获取Nacos的Data ID
     *
     * @param app
     * @return
     */
    protected abstract String getDataId(String app);


    public Converter<String, List<T>> getRuleEntityDecoder() {
        return ruleEntityDecoder;
    }

    public void setRuleEntityDecoder(Converter<String, List<T>> ruleEntityDecoder) {
        this.ruleEntityDecoder = ruleEntityDecoder;
    }

    public long getTimeoutMs() {
        return timeoutMs;
    }

    public void setTimeoutMs(long timeoutMs) {
        this.timeoutMs = timeoutMs;
    }
}
